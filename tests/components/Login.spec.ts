import Login from '@/views/auth/login/index.vue'
import { mount } from '@vue/test-utils'
import ElementPlus from 'element-plus'
import { SvgIcon } from '@/icons'
import { createStore } from 'vuex'

const store = createStore({
  state: {
    //
  },
  mutations: {
    //
  },
})

jest.mock('vue-router', () => ({
  useRouter: () => ({
    push: jest.fn(),
  }),
}))

describe('Login.vue', () => {
  it('submits a form', async () => {
    const wrapper = mount(Login, {
      global: {
        plugins: [ElementPlus, store, SvgIcon],
      },
    })
    // get the fields
    const username = wrapper.find('input[name="username"]')
    const password = wrapper.find('input[name="password"]')
    // try to fill the fields
    await username.setValue('radiologist')
    await password.setValue('password12345')
    const vm = wrapper.vm as any
    // do assertions
    expect(vm.$el.querySelector('input[name="username"]').value).toBe(
      'radiologist'
    )
    expect(vm.$el.querySelector('input[name="password"]').value).toBe(
      'password12345'
    )
  })
})
