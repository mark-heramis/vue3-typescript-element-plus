// vite.config.ts
import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import mockServer from 'vite-plugin-mock-server'
import VitePluginElementPlus from 'vite-plugin-element-plus'

/*  Docs: https://vitejs.dev/config/
 *
 *  Issue Fixed: Use env files inside vite config
 *  Source: https://stackoverflow.com/questions/66389043/how-can-i-use-vite-env-variables-in-vite-config-js
 */
export default ({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) }
  return defineConfig({
    mode: process.env.VITE_APP_ENV,
    build: {
      rollupOptions: {
        external: [pathResolve('node_modules/element-plus')],
      },
    },
    plugins: [
      vue(),
      VitePluginElementPlus({
        // if you need to use the *.scss source file, you need to uncomment this comment
        // useSource: true
        format: mode === 'development' ? 'esm' : 'cjs',
      }),
      /*
      mockServer({
        logLevel: 'info',
        urlPrefixes: ['/api/'],
        mockRootDir: './mock',
        mockJsSuffix: '.mock.js',
        mockTsSuffix: '.mock.ts',
        noHandlerResponse404: true,
        mockModules: [],
      }),
      */
    ],
    define: {
      'process.platform': false,
    },
    resolve: {
      alias: [
        {
          find: '@',
          replacement: pathResolve('src'),
        },
        {
          find: '@modules',
          replacement: pathResolve('./node_modules'),
        },
      ],
    },
    server: {
      port: parseInt(process.env.VITE_DEV_PORT),
      proxy: {
        [process.env.VITE_SERVER_BASEURL]: {
          target: `
            ${process.env.VITE_SERVER_PROTOCOL}://
            ${process.env.VITE_SERVER_URL}:
            ${process.env.VITE_SERVER_PORT}
            ${process.env.VITE_SERVER_BASEURL}
          `.replace(/\s+/g, ''),
          changeOrigin: true,
          ws: true,
          rewrite: (path) => path.replace(process.env.VITE_SERVER_BASEURL, ''),
        },
      },
      watch: {
        usePolling: true,
      },
    },
  })
}

const pathResolve = (dir: string): any => resolve(__dirname, '.', dir)
