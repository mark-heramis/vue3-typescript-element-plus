import * as http from 'http'
import { Connect } from 'vite'

const mocks: MockHandler[] = [
  {
    pattern: '/api/login',
    method: 'POST',
    handle: async (request, response) => {
      let body = ''
      await new Promise((resolve) => {
        request.on('data', (chunk) => {
          body += chunk
        })
        request.on('end', () => resolve(undefined))
      })

      response.setHeader('Content-Type', 'application/json')
      response.statusCode = 200
      response.end(`${body}`)
    },
  },
]

export default mocks
