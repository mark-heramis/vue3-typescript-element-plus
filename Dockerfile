FROM node:latest
LABEL maintainer Mark <chumheramis@gmail.com>

COPY . /var/www/html

WORKDIR /var/www/html

COPY ./start-container /usr/bin/start-container
RUN chmod +x /usr/bin/start-container

EXPOSE 5000

ENTRYPOINT ["start-container"]