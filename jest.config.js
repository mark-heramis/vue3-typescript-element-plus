/*  Jest would not handle vite's import.meta properly.
 *  As a work around, the following settings below is adopted. 
 *  Source: https://github.com/vitejs/vite/issues/1149#issuecomment-775033930
 */
module.exports = {
  moduleFileExtensions: [
    'js',
    'ts',
    'json',
    'vue'
  ],
  transform: {
    '.*\\.(vue|scss|css|ico)$': 'vue-jest',
    '.*\\.(ts)$': 'ts-jest'
  },
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.(js|ts)$',
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1"
  },
  globals: {
    'vue-jest': {
      babelConfig: true,
    },
    'ts-jest': {
      babelConfig: true,
    }
  }
}
