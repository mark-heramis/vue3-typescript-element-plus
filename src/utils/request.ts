import axios from 'axios'
import { AxiosResponse, AxiosError } from 'axios'
// import { ElMessage, ElNotification } from 'element-plus'
import { getToken } from './cookies'

const service = axios.create({
  // url = request url + base url
  baseURL: import.meta.env.VITE_SERVER_BASEURL,
  timeout: parseInt(import.meta.env.VITE_REQUEST_TIMEOUT),
  // withCredentials: true // send cookies when cross-domain requests
})

// Request interceptors 
service.interceptors.request.use(
  (config) => {
    // Add X-Access-Token header to every request, you can add other custom headers here
    const userToken = getToken()
    if (userToken) {
      config.headers.Authorization = `Bearer ${userToken}`
    }
    return config
  },
  (error) => {
    Promise.reject(error)
  }
)

// Response Interceptors
service.interceptors.response.use(
  (response: AxiosResponse) => {
    if (response.data) {
      /*
      if (response.notification) {
        ElNotification({
          title: response.notification.title,
          message: response.notification.message,
          duration: 3500,
        })
      }
      */
      return response
    }
    return Promise.reject(response)
  },
  (error: AxiosError) => {
    /*
    ElMessage.error({
      message: error.response.errors.message || 'Error',
      type: 'error',
      duration: 3500,
      showClose: true,
    })
    */
    return Promise.reject(error)
  }
)

export default service
