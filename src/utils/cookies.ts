import Cookies from 'js-cookie'

// App
const sidebarStatusKey = 'sidebar_status'
export const getSidebarStatus = (): string | undefined => {
  return Cookies.get(sidebarStatusKey)
}
export const setSidebarStatus = (sidebarStatus: string): string | undefined => {
  return Cookies.set(sidebarStatusKey, sidebarStatus)
}

const languageKey = 'language'
export const getLanguage = (): string | undefined => {
  return Cookies.get(languageKey)
}
export const setLanguage = (language: string): string | undefined => {
  return Cookies.set(languageKey, language)
}

const sizeKey = 'size'
export const getSize = (): string | undefined => {
  return Cookies.get(sizeKey)
}
export const setSize = (size: string): string | undefined => {
  return Cookies.set(sizeKey, size)
}

// User
const tokenKey = 'vue_typescript_admin_access_token'
export const getToken = (): string | undefined => {
  return Cookies.get(tokenKey)
}
export const setToken = (token: string): string | undefined => {
  return Cookies.set(tokenKey, token)
}
export const removeToken = (): void => {
  Cookies.remove(tokenKey)
}
