// Import plugins that you want to use
// Detail plugins list see: https://www.tiny.cloud/apps/#core-plugins
// Custom builds see: https://www.tiny.cloud/get-tiny/custom-builds/
export const plugins = [`
  advlist anchor autolink autoresize autosave 
  charmap code codesample 
  directionality 
  emoticons 
  fullpage fullscreen 
  help hr 
  image imagetools insertdatetime 
  link lists 
  media 
  nonbreaking noneditable 
  pagebreak paste preview print 
  quickbars 
  save searchreplace spellchecker 
  tabfocus table template textpattern 
  visualblocks visualchars 
  wordcount 
  `
]

// Here is the list of toolbar control components
// Details see: https://www.tinymce.com/docs/advanced/editor-control-identifiers/#toolbarcontrols
export const toolbar = [
  // upper toolbar buttons
  `
  undo redo | 
  bold italic underline forecolor backcolor fontselect fontsizeselect formatselect | 
  alignleft aligncenter alignright alignjustify | 
  outdent indent |
  bullist numlist | 
  `,
  // lower toolbar buttons
  `
  searchreplace code codesample |  
  blockquote strikethrough removeformat subscript superscript | 
  hr link image charmap anchor pagebreak insertdatetime media table emoticons charmap |
  fullpage preview save print |
  fullscreen help
  `
]


// Define custom shortcuts here
export const shortcuts = (editor: any) => {
    // Alt+o (Ordered List shortcut)
    editor.addShortcut(
      'alt+o',
      'Orderd List',
      () => {
        editor.execCommand('InsertOrderedList')
      }
    )
    // Alt+u (Unordered List shortcut)
    editor.addShortcut(
      'alt+u',
      'Unordered List',
      () => {
        editor.execCommand('InsertUnorderedList')
      }
    )
}
