import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

/* Layout */
import Layout from '@/layout/index.vue'

export const constantRoutes: RouteRecordRaw[] = [
  {
    path: '/login',
    component: () => import( '@/views/auth/login/index.vue' )
  },
  {
    path: '/register',
    component: () => import( '@/views/auth/register/index.vue' )
  },
  {
    path: '/404' ,
    component: () => import('@/views/error-page/404.vue'),
    meta: { hidden: true }
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: 'Dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index.vue'),
        name: 'Dashboard',
        meta: {
          title: 'dashboard',
          icon: 'dashboard',
          affix: true
        }
      },
    ]
  },
  {
    path: '/settings',
    component: Layout,
    redirect: '/settings/index',
    meta: { hidden: true },
    children: [
      {
        path: 'index',
        component: () => import('@/views/settings/index.vue'),
        name: 'AccountSettings',
        meta: {
          title: 'settings',
          icon: 'user',
          noCache: true
        }
      }
    ]
  }
]

export const asyncRoutes: RouteRecordRaw[] = [
  {
    path: '/role',
    component: Layout,
    redirect: '/role/index',
    meta: {
      title: 'roles',
      icon: 'peoples',
      roles: ['administrator'],
      alwaysShow: true
    },
    children: [
      {
        path: 'index',
        component: () => import('@/views/role/index.vue'),
        name: 'Roles',
        meta: {
          title: 'roleIndex',
          roles: ['administrator'],
        }
      }
    ]
  },
  {
      path: '/user',
      component: Layout,
      redirect: '/user/index',
      meta: {
        title: 'users',
        icon: 'user-compact',
        roles: ['administrator'],
        alwaysShow: true
      },
      children: [
        {
          path: 'index',
          component: () => import('@/views/user/index.vue'),
          meta: {
            title: 'userIndex',
            roles: ['administrator'],
          }
        },
        {
          path: 'create',
          component: () => import('@/views/user/create.vue'),
          meta: {
            title: 'userCreate',
            roles: ['administrator'],
          }
        },
        {
          path: ':userId',
          component: () => import('@/views/user/create.vue'),
          props: true,
          name: 'UserEdit',
          meta: {
            title: 'userEdit',
            roles: ['administrator'],
            hidden: true,
          }
        },
      ]
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/404',
    name: 'NotFound',
    meta: { hidden: true }
  }
]

const router = createRouter({
    history: createWebHistory(),
    routes: constantRoutes,
    scrollBehavior( to, from, savedPosition ) {
      if ( savedPosition ) {
        return savedPosition
      } else {
        return { left: 0, top: 0 }
      }
    }
})

export default router
