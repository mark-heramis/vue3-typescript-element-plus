/// <reference types="vite/client" />

interface ImportMetaEnv {
  VITE_APP_ENV: string
  VITE_SERVER_URL: string
  VITE_SERVER_PORT: string
  VITE_SERVER_BASEURL: string
  VITE_SERVER_PROTOCOL: string
  VITE_DEV_PORT: string
  VITE_DEV_URL: string
  VITE_REQUEST_TIMEOUT: string
}
