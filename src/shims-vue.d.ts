declare module '*.vue' {
  import { DefineComponent } from 'vue'
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>
  export default component
}

declare module 'vue/dist/vue.esm-bundler'

declare module 'element-plus/lib/locale/lang/*' {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  export const elementLocale: any
}

declare module 'element-plus/lib/*' {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  export const Element: any
}

declare module '*.scss' {
  export const variables: { [className: string]: string }
}
