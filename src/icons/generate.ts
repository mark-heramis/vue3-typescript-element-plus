/*  https://github.com/element-plus/element-plus-icons
 *  https://github.com/svg/svgo
 *
 *  Converts SVG file into Vue 3 Component
 *
 *  Requires ts-node to transpile typescript code on the fly.
 *  npm install -g ts-node
 */

interface IconData {
  width?: number | string
  height?: number | string
  viewBox: string
  path: string
}

interface OptimizedSvg {
  data: string
  info: {
    width: string
    height: string
  }
  path?: string
}

const fs = require('fs')
const path = require('path')
const { optimize } = require('svgo')

const ROOT = path.resolve(__dirname, './svg')
const outDir = path.resolve(__dirname, './components')

// SVGO config
const config = {
  plugins: [
    'cleanupAttrs',
    'removeDoctype',
    'removeXMLProcInst',
    'removeComments',
    'removeMetadata',
    'removeTitle',
    'removeDesc',
    'removeUselessDefs',
    'removeEditorsNSData',
    'removeEmptyAttrs',
    'removeHiddenElems',
    'removeEmptyText',
    'removeEmptyContainers',
    // 'removeViewBox',
    'cleanupEnableBackground',
    'convertStyleToAttrs',
    'convertColors',
    'convertPathData',
    'convertTransform',
    'removeUnknownsAndDefaults',
    'removeNonInheritableGroupAttrs',
    'removeUselessStrokeAndFill',
    'removeUnusedNS',
    'cleanupIDs',
    'cleanupNumericValues',
    'moveElemsAttrsToGroup',
    'moveGroupAttrsToElems',
    'collapseGroups',
    // 'removeRasterImages',
    'mergePaths',
    'convertShapeToPath',
    'sortAttrs',
    'removeDimensions',
    { name: 'removeAttrs', params: { attrs: '(stroke|fill|class)' } },
  ],
}

const icons: string[] = fs.readdirSync(ROOT)

const gen = async(filename: string) => {
  /*  Read svg file data
   *
   */
  const content: any = await fs.promises.readFile(
    path.resolve(ROOT, filename),
    {
      encoding: 'utf-8',
    }
  )

  /*  Set file name
   *
   */
  const basename = filename.split('.svg').shift()

  let fileName = ''
  if (basename) fileName = basename.split('-').map(toLower).join('-')

  /*  Extract needed data from the svg fie
   *
   */
  const optimized: OptimizedSvg = optimize(content, config)
  const raw: OptimizedSvg = optimize(content, config)
  const svgPath = optimized.data
    .replace(/<svg[^>]+>/gi, '')
    .replace(/<\/svg>/gi, '')
  const viewBox = getViewBox(optimized)

  const data: IconData = {
    width: raw.info.width,
    height: raw.info.height,
    viewBox: viewBox,
    path: svgPath,
  }

  /*  Transform and write the generated
   *  data to disk
   */
  const transformed = transform(data, fileName)

  return writeToDisk(transformed, fileName)
}

const writeToDisk = (content: any, componentName: string, extension = 'ts') => {
  const targetFile = path.resolve(outDir, `./${componentName}.${extension}`)

  return fs.promises.writeFile(targetFile, content, {
    encoding: 'utf-8',
  })
}

const toLower = (word: string) => {
  return word.toLocaleLowerCase()
}

// get svg viewbox
const getViewBox = (svgoResult: OptimizedSvg) => {
  const viewBoxMatch = svgoResult.data.match(
    /viewBox="([-\d.]+\s[-\d.]+\s[-\d.]+\s[-\d.]+)"/
  )
  let viewBox = '0 0 200 200'

  if (viewBoxMatch && viewBoxMatch.length > 1) {
    viewBox = viewBoxMatch[1]
  } else if (svgoResult.info.height && svgoResult.info.width) {
    viewBox = `0 0 ${svgoResult.info.width} ${svgoResult.info.height}`
  }

  return viewBox
}

const transform = (data: IconData, componentName: string) => {
  return `export default {
  name: '${componentName}',
  data: {
    width: ${data.width}, 
    height: ${data.height}, 
    viewBox: '${data.viewBox}',
    path: '${data.path}'
  }
}
`
}

// transpile each svg extacted data into icon component folder
icons.forEach((icon: string) => {
  gen(icon).then(() => console.log(`finished processing file...${icon}`))
})
