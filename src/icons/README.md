# [vue3-svgicon](https://github.com/MMF-FE/svgicon/tree/master/packages/vue3-svgicon)

* All svg icon files will be imported using [Vite Glob Import(eager loading)](https://vitejs.dev/guide/features.html#glob-import)
* To add new icons, simply put svg files to `src/icons/svg` folder. Make sure there are no duplicates.
* Svg icons are ready to use by assigning value to `name` prop: `<svg-icon name="name-of-svg-icon" />`
* Lastly, run `yarn icon:gen` or `npm run icon:gen` to generate icon components.
