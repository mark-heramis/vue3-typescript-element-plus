export default {
  name: 'platform',
  data: {
    width: 200, 
    height: 200, 
    viewBox: '0 0 1024 1024',
    path: '<path d="M448 832v-64h128v64h192v64H256v-64h192zM128 704V128h768v576H128z"/>'
  }
}
