export default {
  name: 'caret-bottom',
  data: {
    width: 200, 
    height: 200, 
    viewBox: '0 0 1024 1024',
    path: '<path d="m192 384 320 384 320-384z"/>'
  }
}
