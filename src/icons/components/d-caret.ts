export default {
  name: 'd-caret',
  data: {
    width: 200, 
    height: 200, 
    viewBox: '0 0 1024 1024',
    path: '<path d="m512 128 288 320H224l288-320zM224 576h576L512 896 224 576z"/>'
  }
}
