export default {
  name: 'minus',
  data: {
    width: 200, 
    height: 200, 
    viewBox: '0 0 1024 1024',
    path: '<path d="M128 544h768a32 32 0 1 0 0-64H128a32 32 0 0 0 0 64z"/>'
  }
}
