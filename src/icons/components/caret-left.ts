export default {
  name: 'caret-left',
  data: {
    width: 200, 
    height: 200, 
    viewBox: '0 0 1024 1024',
    path: '<path d="M672 192 288 511.936 672 832z"/>'
  }
}
