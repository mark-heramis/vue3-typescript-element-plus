export default {
  name: 'caret-right',
  data: {
    width: 200, 
    height: 200, 
    viewBox: '0 0 1024 1024',
    path: '<path d="M384 192v640l384-320.064z"/>'
  }
}
