export default {
  name: 'semi-select',
  data: {
    width: 200, 
    height: 200, 
    viewBox: '0 0 1024 1024',
    path: '<path d="M128 448h768q64 0 64 64t-64 64H128q-64 0-64-64t64-64z"/>'
  }
}
