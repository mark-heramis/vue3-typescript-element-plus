export default {
  name: 'caret-top',
  data: {
    width: 200, 
    height: 200, 
    viewBox: '0 0 1024 1024',
    path: '<path d="M512 320 192 704h639.936z"/>'
  }
}
