export default {
  name: 'bottom-right',
  data: {
    width: 200, 
    height: 200, 
    viewBox: '0 0 1024 1024',
    path: '<path d="M352 768a32 32 0 1 0 0 64h448a32 32 0 0 0 32-32V352a32 32 0 0 0-64 0v416H352z"/><path d="M777.344 822.656a32 32 0 0 0 45.312-45.312l-544-544a32 32 0 0 0-45.312 45.312l544 544z"/>'
  }
}
