import { App, h } from 'vue'

interface ISvgData {
  name: string
  data: {
    viewBox: string
    path: string
    width: string | number
    height: string | number
  }
}

const iconsList = import.meta.globEager('./components/*')

const icons = () => {
  const result: any[] = []
  for (const icon in iconsList) {
    result.push(iconsList[icon].default)
  }
  return result
}
/**
 * No Documentation
 * @todo maybe make the `install` function the main export since there's no other function/object being exported anyways.
 */
export const SvgIcon = {
  /**
   * No Documentation
   *
   * @param app
   */
  install: (app: App) => {
    app.component('svg-icon', {
      props: {
        name: {
          required: true,
          type: String,
        },
        width: {
          type: String,
          default: '1em',
        },
        height: {
          type: String,
          default: '1em',
        },
      },
      render() {
        const result: ISvgData = icons().find((icon: any) => {
          return icon.name === this.$props.name
        })
        return h('svg', {
          viewBox: result.data.viewBox,
          innerHTML: result.data.path,
          width: this.$props.width,
          height: this.$props.height,
          class: 'svg-icon svg-fill',
        })
      },
    })
  },
}
