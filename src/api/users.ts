import request from '@/utils/request'

export const login = (data: any) =>
  request({
    url: '/auth/login',
    method: 'post',
    data,
  })
export const register = (data: any) =>
  request({
    url: '/auth/register',
    method: 'post',
    data,
  })

export const getUserInfo = (data: any) =>
  request({
    url: '/auth/me',
    method: 'get',
    data,
  })

export const validateCode = (data: any) =>
  request({
    url: `/auth/mfa/verify`,
    method: 'post',
    data,
  })

export const getUsers = (params: any) =>
  request({
    url: '/user',
    method: 'get',
    params,
  })

export const getUsersByUrlQuery = (params: any) =>
  request({
    url: `/user?${params.query}=${params.value}`,
    method: 'get',
    params,
  })

export const getUserById = (id: number) =>
  request({
    url: `/user/${id}`,
    method: 'get',
  })

export const updateUser = (id: number, data: any) =>
  request({
    url: `/user/${id}`,
    method: 'put',
    data,
  })

export const updateUserRole = (id: number, data: any) =>
  request({
    url: `/user/${id}/role`,
    method: 'put',
    data,
  })

export const deleteUser = (id: number) =>
  request({
    url: `/user/${id}`,
    method: 'delete',
  })

export const deleteUserRole = (id: number, data: any) =>
  request({
    url: `/user/${id}/role`,
    method: 'delete',
    data,
  })

export const getQRCode = (id: number) =>
  request({
    url: `/user/${id}/mfa`,
    method: 'get',
  })

export const logout = () =>
  request({
    url: '/user/logout',
    method: 'post',
  })

export const paginate = (params: any) =>
  request({
    url: `/user?page=${params.page}`,
    method: 'get',
    params,
  })
