interface ISettings {
  title: string // Overrides the default title
  showSettings: boolean // Controls settings panel display
  showTagsView: boolean // Controls tagsview display
  showSidebarLogo: boolean // Controls siderbar logo display
  sidebarTextTheme: boolean // If true, will change active text color for sidebar based on theme
  fixedHeader: boolean // If true, will fix the header component
}

// You can customize below settings :)
const settings: ISettings = {
  title: import.meta.env.VITE_TITLE as string,
  showSettings: true,
  showTagsView: true,
  fixedHeader: false,
  showSidebarLogo: true,
  sidebarTextTheme: true,
}

export default settings
