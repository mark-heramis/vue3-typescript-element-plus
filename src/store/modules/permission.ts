import { MutationTree, ActionTree, GetterTree } from 'vuex'
import { asyncRoutes, constantRoutes } from '@/router'
import { RouteRecordNormalized, RouteRecordRaw } from 'vue-router'


/*
 *  TYPES
 */
export interface IRouteRecordNormalized extends RouteRecordNormalized {
  meta: {
    roles?: string[]
  }
}

export interface IPermissionState {
  routes: RouteRecordRaw[]
  dynamicRoutes: RouteRecordRaw[]
}

const hasPermission = (roles: string[], route: IRouteRecordNormalized) => {
  if ( route.meta ) {
    return roles.some(role => {
      if( route.meta.roles ) return route.meta.roles.includes(role)
    })
  } else {
    return true
  }
}

export const filterAsyncRoutes = (routes: RouteRecordRaw[], roles: string[]) => {
  const res: RouteRecordRaw[] = []
  routes.forEach((route: any) => {
    const r = { ...route }
    if ( hasPermission(roles, r) ) {
      if (r.children) {
        r.children = filterAsyncRoutes(r.children, roles)
      }
      res.push(r)
    }
  })
  return res
}
/*
 *  STATE
 */
const state: IPermissionState = {
  routes: [],
  dynamicRoutes: []
}
/*
 *  GETTER
 */
const getters: GetterTree<IPermissionState, unknown> = {}
/*
 *  MUTATION
 */
const mutations: MutationTree<IPermissionState> = {
  SET_ROUTES: (state, routes: RouteRecordRaw[]) => {
    state.routes = constantRoutes.concat(routes)
    state.dynamicRoutes = routes
  }
}
/*
 *  ACTION
 */
const actions: ActionTree<IPermissionState, unknown> = {
  GenerateRoutes: ({ commit }, roles: string[]) => {
    let accessedRoutes: any
    if ( roles.includes('administrator') ) {
      accessedRoutes = asyncRoutes 
    } else {
      accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
    }
    commit('SET_ROUTES', accessedRoutes)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
