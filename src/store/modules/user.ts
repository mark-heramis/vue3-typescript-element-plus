import { login, getUserInfo } from '@/api/users'
import { getToken, setToken, removeToken } from '@/utils/cookies'
import { ActionTree, GetterTree, MutationTree } from 'vuex'

/*
 *  TYPES
 */
export interface IUserState {
  token: string
  id: string | number
  first_name: string
  last_name: string
  username: string
  avatar?: string
  introduction?: string
  roles: string[]
  email: string
  phone_number?: string | number
  country_code?: string | number
}

/*
 *  STATE
 */
const state: IUserState = {
  token: getToken() || '',
  id: 0,
  username: '',
  first_name: '',
  last_name: '',
  avatar: '',
  introduction: '',
  roles: [],
  email: '',
}
/*
 *  GETTER
 */
const getters: GetterTree<IUserState, unknown> = {}
/*
 *  MUTATION
 */
const mutations: MutationTree<IUserState> = {
  SET_TOKEN: (state, token: string) => {
    state.token = token
  },
  SET_ID: (state, id: number) => {
    state.id = id
  },
  SET_FIRSTNAME: (state, first_name: string) => {
    state.first_name = first_name
  },
  SET_LASTNAME: (state, last_name: string) => {
    state.last_name = last_name
  },
  SET_USERNAME: (state, username: string) => {
    state.username = username
  },
  SET_AVATAR: (state, avatar: string) => {
    state.avatar = avatar
  },
  SET_INTRODUCTION: (state, introduction: string) => {
    state.introduction = introduction
  },
  SET_ROLES: (state, roles: string[]) => {
    state.roles = roles
  },
  SET_EMAIL: (state, email: string) => {
    state.email = email
  },
  SET_PHONE_NUMBER: (state, phone_number: string | number) => {
    state.phone_number = phone_number
  },
  SET_COUNTRY_CODE: (state, country_code: string | number) => {
    state.country_code = country_code
  },
}
/*
 *  ACTION
 */
const actions: ActionTree<IUserState, unknown> = {
  Login: async(
    { commit },
    userInfo: { username: string, password: string }
  ) => {
    let username = userInfo.username
    const password = userInfo.password
    username = username.trim()
    const [data, error] = await login({ username, password }).then(
      (response) => {
        return [response.data.data, response.data.error]
      }
    )
    if (!error) {
      setToken(data.token)
      commit('SET_TOKEN', data.token)
    }
    return data
  },
  ResetToken: ({ commit }) => {
    removeToken()
    commit('SET_TOKEN', '')
    commit('SET_ROLES', [])
  },
  GetUserInfo: async({ commit, state }) => {
    if (state.token === '') throw Error('GetUserInfo: token is undefined!')
    const [user, error] = await getUserInfo({}).then((response) => [
      response.data.data,
      response.data.error,
    ])
    if (error) throw Error('Verification failed, please Login again.')
    const role: string[] = []
    user.roles.forEach((item: any) => {
      role.push(item.slug)
    })
    const img = new URL('../../assets/avatar.gif', import.meta.url)
    commit('SET_ROLES', role)
    commit('SET_ID', user.id)
    commit('SET_FIRSTNAME', user.first_name)
    commit('SET_LASTNAME', user.last_name)
    commit('SET_USERNAME', user.username)
    commit('SET_EMAIL', user.email)
    commit('SET_AVATAR', img.href)
    commit('SET_PHONE_NUMBER', user.phone_number)
    commit('SET_COUNTRY_CODE', user.country_code)
  },

  LogOut: ({ commit, state }) => {
    if (state.token === '') throw Error('LogOut: token is undefined!')
    removeToken()
    commit('SET_TOKEN', '')
    commit('SET_ROLES', [])
  },
}

export default {
  state,
  getters,
  mutations,
  actions,
}
