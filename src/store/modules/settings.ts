import { ActionTree, GetterTree, MutationTree } from 'vuex'
import elementVariables from '@/styles/modules/element-variables.module.scss'
import defaultSettings from '@/settings'


/*
 *  TYPES
 */
export interface ISettingsState {
  theme: string
  fixedHeader: boolean
  showSettings: boolean
  showTagsView: boolean
  showSidebarLogo: boolean
  sidebarTextTheme: boolean
}
/*
 *  STATE
 */
const state: ISettingsState = {
  theme: elementVariables.theme,
  fixedHeader: false,
  showSettings: defaultSettings.showSettings,
  showTagsView: defaultSettings.showTagsView,
  showSidebarLogo: defaultSettings.showSidebarLogo,
  sidebarTextTheme: defaultSettings.sidebarTextTheme
}
/*
 *  GETTER
 */
const getters: GetterTree<ISettingsState, unknown> = {}
/*
 *  MUTATION
 */
const mutations: MutationTree<ISettingsState> = {
  CHANGE_SETTING: (state, payload: { key: string, value: any }) => {
    const { key, value } = payload
    if (Object.prototype.hasOwnProperty.call(this, key)) {
      (this as any)[key] = value
    }
  }
}
/*
 *  ACTION
 */
const actions: ActionTree<ISettingsState, unknown> = {
  ChangeSetting: ({ commit }, payload: { key: string, value: any }) => {
    commit('CHANGE_SETTING', payload)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
