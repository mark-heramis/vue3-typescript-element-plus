import { ActionTree, GetterTree, MutationTree } from 'vuex'
import { getSidebarStatus, getSize, setLanguage, setSidebarStatus, setSize } from '@/utils/cookies'


/*
 *  TYPES
 */
export enum DeviceType {
  Mobile,
  Desktop
}

export interface IAppState {
  device: DeviceType
  sidebar: {
    opened: boolean
    withoutAnimation: boolean
  }
  language: string
  size: string
}
/*
 *  ACTION
 */
const state: IAppState = {
  sidebar: { opened: getSidebarStatus() !== 'closed', withoutAnimation: false },
  device: DeviceType.Desktop,
  language: 'en',
  size: getSize() || 'medium'
}
/*
 *  GETTER
 */
const getters: GetterTree<IAppState, unknown> = {}
/*
 *  MUTATION
 */
const mutations: MutationTree<IAppState> = {

  TOGGLE_SIDEBAR: (state, withoutAnimation: boolean) => {
    state.sidebar.opened = ! state.sidebar.opened 
    state.sidebar.withoutAnimation = withoutAnimation
    if (state.sidebar.opened) {
      setSidebarStatus('opened')
    } else {
      setSidebarStatus('closed')
    }
  },

  CLOSE_SIDEBAR: (state, withoutAnimation: boolean) => {
    state.sidebar.opened = false
    state.sidebar.withoutAnimation = withoutAnimation
    setSidebarStatus('closed')
  },

  TOGGLE_DEVICE: (state, device: DeviceType) => {
    state.device = device
  },

  SET_LANGUAGE: (state, language: string) => {
    state.language = language
    setLanguage(state.language)
  },

  SET_SIZE: (state, size: string) => {
    state.size = size
    setSize(state.size)
  }

}
/*
 *  ACTION
 */
const actions: ActionTree<IAppState, unknown> = {

  ToggleSidebar: ({ commit }, withoutAnimation: boolean) => {
    commit('TOGGLE_SIDEBAR', withoutAnimation)
  },

  CloseSidebar: ({ commit }, withoutAnimation: boolean) => {
    commit('CLOSE_SIDEBAR', withoutAnimation)
  },
  
  ToggleDevice: ({ commit }, device: DeviceType) => {
    commit('TOGGLE_DEVICE', device)
  },
  
  SetLanguage: ({ commit }, language: string) => {
    commit('SET_LANGUAGE', language)
  },

  SetSize: ({ commit }, size: string) => {
    commit('SET_SIZE', size)
  }

}

export default {
  state,
  getters,
  mutations,
  actions
}
