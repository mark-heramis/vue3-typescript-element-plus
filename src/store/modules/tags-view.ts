import { StoreWritable } from '@/store/_base'
import { RouteLocationNormalized, RouteRecordName, RouteMeta } from 'vue-router'

export interface ITagView extends Partial<RouteLocationNormalized> {
  // title?: RouteMeta
  meta: RouteMeta
}

export interface ITagsViewState {
  visitedViews: ITagView[]
  cachedViews: (RouteRecordName | undefined)[]
}

class TagsView extends StoreWritable<ITagsViewState> {
  protected data(): ITagsViewState {
    return {
      visitedViews: [] as ITagView[],
      cachedViews: [] as (string | undefined)[]
    }
  }

  private ADD_VISITED_VIEW(view: ITagView) {
    if ( this.state.visitedViews.some(v => v.path === view.path) ) return
    this.state.visitedViews.push(
      Object.assign({}, view, {
        title: view.meta.title || 'no-name'
      })
    )
  }

  private ADD_CACHED_VIEW(view: ITagView) {
    if (view.name === null) return
    if (this.state.cachedViews.includes(view.name)) return
    if (!view.meta.noCache) {
      this.state.cachedViews.push(view.name)
    }
  }

  private DEL_VISITED_VIEW(view: ITagView) {
    for (const [i, v] of this.state.visitedViews.entries()) {
      if (v.path === view.path) {
        this.state.visitedViews.splice(i, 1)
        break
      }
    }
  }

  private DEL_CACHED_VIEW(view: ITagView) {
    if (view.name === null) return
    const index = this.state.cachedViews.indexOf(view.name)
    index > -1 && this.state.cachedViews.splice(index, 1)
  }

  private DEL_OTHERS_VISITED_VIEWS(view: ITagView) {
    this.state.visitedViews = this.state.visitedViews.filter(v => {
      return v.meta.affix || v.path === view.path
    })
  }

  private DEL_OTHERS_CACHED_VIEWS(view: ITagView) {
    if (view.name === null) return
    const index = this.state.cachedViews.indexOf(view.name)
    if (index > -1) {
      this.state.cachedViews = this.state.cachedViews.slice(index, index + 1)
    } else {
      // if index = -1, there is no cached tags
      this.state.cachedViews = []
    }
  }

  private DEL_ALL_VISITED_VIEWS() {
    // keep affix tags
    const affixTags = this.state.visitedViews.filter(tag => tag.meta.affix)
    this.state.visitedViews = affixTags
  }

  private DEL_ALL_CACHED_VIEWS() {
    this.state.cachedViews = []
  }

  private UPDATE_VISITED_VIEW(view: ITagView) {
    for (let v of this.state.visitedViews) {
      if (v.path === view.path) {
        v = Object.assign(v, view)
        break
      }
    }
  }

  public addView(view: ITagView) {
    this.ADD_VISITED_VIEW(view)
    this.ADD_CACHED_VIEW(view)
  }

  public addVisitedView(view: ITagView) {
    this.ADD_VISITED_VIEW(view)
  }

  public delView(view: ITagView) {
    this.DEL_VISITED_VIEW(view)
    this.DEL_CACHED_VIEW(view)
  }

  public delCachedView(view: ITagView) {
    this.DEL_CACHED_VIEW(view)
  }

  public delOthersViews(view: ITagView) {
    this.DEL_OTHERS_VISITED_VIEWS(view)
    this.DEL_OTHERS_CACHED_VIEWS(view)
  }

  public delAllViews() {
    this.DEL_ALL_VISITED_VIEWS()
    this.DEL_ALL_CACHED_VIEWS()
  }

  public delAllCachedViews() {
    this.DEL_ALL_CACHED_VIEWS()
  }

  public updateVisitedView(view: ITagView) {
    this.UPDATE_VISITED_VIEW(view)
  }
}

export const TagsViewModule: TagsView = new TagsView()
