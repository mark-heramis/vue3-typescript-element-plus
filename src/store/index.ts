import { createLogger, createStore } from 'vuex'
import app from './modules/app'
import permission from './modules/permission'
import setting from './modules/settings'
import user from './modules/user'


const debug = import.meta.env.VITE_APP_ENV !== 'production'

export default createStore({
  modules: {
    app,
    permission,
    setting,
    user
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
