export interface IScssVariables {
  theme: string
}

export const elementVariables: IScssVariables

export default elementVariables
