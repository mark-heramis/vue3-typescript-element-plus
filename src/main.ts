import { createApp } from 'vue/dist/vue.esm-bundler'
import 'normalize.css'
import ElementPlus from 'element-plus'
import '@/styles/index.scss'
import 'element-plus/packages/theme-chalk/src/index.scss'
import App from './App.vue'
import store from '@/store'
import router from '@/router'
import '@/permission'
import { SvgIcon } from './icons'
// Initialize
createApp(App)
  .use(ElementPlus)
  .use(SvgIcon)
  .use(store)
  .use(router)
  .mount('#app')
