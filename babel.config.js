/*  Only used by Jest
 *  Source: https://github.com/vitejs/vite/issues/1149#issuecomment-775033930
 */
module.exports = {
  presets: [
    [
      '@babel/preset-env',
      { targets: { node: 'current' } },
    ],
    'babel-preset-vite',
    '@babel/preset-typescript',
  ],
  plugins: [
    'inline-svg',
    'babel-plugin-transform-vite-meta-env',
    'babel-plugin-transform-vite-meta-glob',
  ],
}
